DROP TABLE IF EXISTS categories;

CREATE TABLE categories
(
    category_id   INTEGER PRIMARY KEY AUTOINCREMENT,
    category_name TEXT NOT NULL,
    created_at    DATETIME,
    updated_at    DATETIME
);

INSERT INTO categories (category_name, created_at, updated_at)
VALUES ('计算机', CURRENT_TIMESTAMP, NULL),
       ('历史', CURRENT_TIMESTAMP, NULL),
       ('人物传记', CURRENT_TIMESTAMP, NULL);


DROP TABLE IF EXISTS books;

CREATE TABLE books
(
    book_id     INTEGER PRIMARY KEY AUTOINCREMENT,
    book_name   TEXT    NOT NULL,
    category_id INTEGER NOT NULL,
    author      TEXT    NOT NULL,
    description TEXT,
    created_at  DATETIME,
    updated_at  DATETIME,
    FOREIGN KEY (category_id) REFERENCES categories (category_id) ON DELETE SET NULL
);

INSERT INTO books(book_name, category_id, author, description, created_at, updated_at)
VALUES ('Python编程：从入门到实践（第3版）', 1, '埃里克·马瑟斯',
        '本书是享誉全球的Python入门书，影响了超过250万读者。', CURRENT_TIMESTAMP,
        NULL),
       ('程序是怎样跑起来的（第3版）', 1, '矢沢久雄', '“计算机组成原理”图解趣味版',
        CURRENT_TIMESTAMP,
        NULL),
       ('Python 3网络爬虫开发实战（第2版）', 1, '崔庆才', 'Python 3 开发网络爬虫入门书',
        CURRENT_TIMESTAMP, NULL),
       ('明朝那些事儿', 2, '当年明月', '主要讲述的是从1344年到1644年这三百年间关于明朝的一些事情。',
        CURRENT_TIMESTAMP, NULL),
       ('长安的荔枝', 2, '马伯庸', '马伯庸历史短小说。', CURRENT_TIMESTAMP, NULL),
       ('透过历史看地理', 2, '李不白', '无', CURRENT_TIMESTAMP, NULL),
       ('邓小平时代', 3, '傅高义', '无', CURRENT_TIMESTAMP, NULL),
       ('史蒂夫·乔布斯传', 3, '艾萨克森', '无', CURRENT_TIMESTAMP, NULL),
       ('大明王朝1566', 2, '刘和平', '无', CURRENT_TIMESTAMP, NULL),
       ('人类简史：从动物到上帝', 2, '尤瓦尔·赫拉利', '无', CURRENT_TIMESTAMP, NULL),
       ('网络是怎样连接的', 1, '户根勤', '无', CURRENT_TIMESTAMP, NULL),
       ('设计模式的艺术', 1, '刘伟', '无', CURRENT_TIMESTAMP, NULL);