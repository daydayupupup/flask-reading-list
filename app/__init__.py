import os

from flask import Flask
from flask_cors import CORS

from app.extensions import db, ma

base_dir = os.path.abspath(os.path.dirname(__file__))


def create_app():
    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(base_dir, 'app.db')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = True

    # SQLAlchemy 要在 Marshmallow 之前初始化
    db.init_app(app)
    ma.init_app(app)

    from app.controllers import book_controller, category_controller

    app.register_blueprint(book_controller.bp)
    app.register_blueprint(category_controller.bp)

    CORS(app)

    @app.route('/test')
    def test():
        return '<h2>Hello world!</h2>'

    return app
