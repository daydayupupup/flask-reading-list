from datetime import datetime

from app.extensions import db, ma


class Category(db.Model):
    __tablename__ = 'categories'

    category_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    category_name = db.Column(db.String, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, onupdate=datetime.now)

    books = db.relationship('Book', backref='category')


class CategorySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Category

    books = ma.Nested('BookSchema', many=True, exclude=('category',))
